package com.example.myapplication.homework4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;

public class Homework4Activity extends AppCompatActivity {

    private EditText etUsername;
    private EditText etPassword;
    private Button load;
    private Button write;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework4);
        initView();
        initClick();
    }

    private void initClick() {
        write.setOnClickListener(v -> {
            saveUserInfo();
        });
        load.setOnClickListener(v -> {
            getUserInfo();
        });
    }

    private void initView() {
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        load = findViewById(R.id.bt_load);
        write = findViewById(R.id.bt_write);
    }

    private void saveUserInfo(){
        SharedPreferences userInfo = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = userInfo.edit();//获取Editor
        //得到Editor后，写入需要保存的数据
        editor.putString("username", etUsername.getText().toString());
        editor.putString("password", etPassword.getText().toString());
        etUsername.setText("");
        etPassword.setText("");
        Toast.makeText(getApplicationContext(), "写入成功",
                Toast.LENGTH_SHORT).show();
        editor.commit();//提交修改
    }
    /**
     * 读取用户信息
     */
    private void getUserInfo(){
        SharedPreferences userInfo = getSharedPreferences("data", MODE_PRIVATE);
        String username = userInfo.getString("username", null);//读取username
        String password = userInfo.getString("password", null);//读取age
        if (username!=null&&password!=null){
            etUsername.setText(username);
            etPassword.setText(password);
            Toast.makeText(getApplicationContext(), "读取成功",
                    Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(getApplicationContext(), "无本地数据",
                    Toast.LENGTH_SHORT).show();
        }
    }
}