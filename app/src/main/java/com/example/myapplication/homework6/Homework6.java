package com.example.myapplication.homework6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myapplication.R;

import java.util.ArrayList;

public class Homework6 extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework6);
        button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(v->{
            ArrayList<MyContacts> allContacts = ContactUtils.getAllContacts(this);
            for (MyContacts i : allContacts){
                Toast.makeText(this,i.toString(),Toast.LENGTH_SHORT).show();
                Log.e("asher",i.toString());
            }
        });
    }
}