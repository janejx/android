package com.example.myapplication.homework1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.homework1.enums.CityEnum;
import com.example.myapplication.homework1.enums.SexEnum;
import com.example.myapplication.homework1.mo.PersonMO;
import com.google.gson.Gson;

public class DisplayActivity extends AppCompatActivity {

    private PersonMO personMO;
    private TextView username;
    private TextView password;
    private TextView sex;
    private TextView hobby;
    private TextView city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_display);
        Intent intent = getIntent();
        String obj = intent.getStringExtra("Obj");
        personMO = new Gson().fromJson(obj, PersonMO.class);
        init();
        show();
    }

    private void show() {
        username.setText(personMO.getUsername());
        password.setText(personMO.getPassword());
        if (personMO.getSex() != null){
            sex.setText(SexEnum.getValueByCode(personMO.getSex()));
        }else {
            sex.setText("未知");
        }
        PersonMO.Hobby hobby = personMO.getHobby();
        String hobbyText = "";
        if (hobby.getLikeDance()){
            hobbyText = "跳舞; ";
        }
        if (hobby.getLikePlayBall()){
            hobbyText = hobbyText + "打球; ";
        }
        if (hobby.getLikeSing()){
            hobbyText = hobbyText + "唱歌; ";
        }
        if (hobbyText.equals("")){
            hobbyText = "无";
        }
        this.hobby.setText(hobbyText);
        city.setText(CityEnum.getValueByCode(personMO.getCity()));
    }

    private void init() {
        username = (TextView) findViewById(R.id.dis_username);
        password = (TextView) findViewById(R.id.dis_password);
        sex = (TextView) findViewById(R.id.dis_sex);
        hobby = (TextView) findViewById(R.id.dis_hobby);
        city = (TextView) findViewById(R.id.dis_city);
    }
}