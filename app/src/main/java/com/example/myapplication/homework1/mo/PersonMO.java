package com.example.myapplication.homework1.mo;

public class PersonMO {
    private String username;
    private String password;
    private Integer sex;
    private Hobby hobby;
    private Integer city;

    public PersonMO() {
        this.hobby = new Hobby();
    }

    public class Hobby{
        Boolean likeSing = false;
        Boolean likeDance = false;
        Boolean likePlayBall = false;

        public Boolean getLikeSing() {
            return likeSing;
        }

        public void setLikeSing(Boolean likeSing) {
            this.likeSing = likeSing;
        }

        public Boolean getLikeDance() {
            return likeDance;
        }

        public void setLikeDance(Boolean likeDance) {
            this.likeDance = likeDance;
        }

        public Boolean getLikePlayBall() {
            return likePlayBall;
        }

        public void setLikePlayBall(Boolean likePlayBall) {
            this.likePlayBall = likePlayBall;
        }

        @Override
        public String toString() {
            return "Hobby{" +
                    "likeSing=" + likeSing +
                    ", likeDance=" + likeDance +
                    ", likePlayBall=" + likePlayBall +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "PersonMO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", sex=" + sex +
                ", hobby=" + hobby +
                ", city=" + city +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Hobby getHobby() {
        return hobby;
    }

    public void setHobby(Hobby hobby) {
        this.hobby = hobby;
    }
}
