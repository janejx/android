package com.example.myapplication.homework1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.R;

public class Homework1Activity extends AppCompatActivity implements View.OnClickListener {

    private View button_login_up;
    private View button_login_in;
    private View username;
    private View password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        button_login_up = findViewById(R.id.button1);
        button_login_in = findViewById(R.id.button2);
        username = findViewById(R.id.editTextTextPersonName1);
        password = findViewById(R.id.editTextTextPersonName2);
        button_login_in.setOnClickListener(this);
        button_login_up.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1:
//                Toast.makeText(this, "注册",
//                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, LoginUpActivity.class);
                startActivity(intent);
                break;
            case R.id.button2:
                Toast.makeText(this, "登录",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }
}