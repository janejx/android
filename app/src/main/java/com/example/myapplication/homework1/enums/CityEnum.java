package com.example.myapplication.homework1.enums;

public enum CityEnum {
    BJ(0,"北京"),SH(1,"上海"),GZ(2,"广州"),
    SZ(3,"深圳"),XN(4,"咸宁")
    ;
    private  final int code;
    private  final String message;

    CityEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static String getValueByCode(Integer code){
        for(CityEnum city:CityEnum.values()){
            if(code.equals(city.getCode())){
                return city.getMessage();
            }
        }
        return  null;
    }
}
