package com.example.myapplication.homework1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.example.myapplication.R;
import com.example.myapplication.homework1.enums.SexEnum;
import com.example.myapplication.homework1.mo.PersonMO;
import com.google.gson.Gson;

import java.util.ArrayList;

public class LoginUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText usernameView;
    private EditText passwordView;
    private View checkBoxDance;
    private View checkBoxPlayBall;
    private View checkBoxSing;
    private View radioMale;
    private PersonMO personMO = new PersonMO();
    private View radioFemale;
    private View buttonLoginUp;
    private Spinner spinner;
    private ArrayList<String> dataList;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_up);
        init();
    }

    private void init() {
        usernameView = (EditText)findViewById(R.id.editTextText_username);
        passwordView = (EditText)findViewById(R.id.editText_password);
        checkBoxDance = findViewById(R.id.checkBox_dance);
        checkBoxPlayBall = findViewById(R.id.checkBox_playBall);
        checkBoxSing = findViewById(R.id.checkBox_sing);
        radioMale = findViewById(R.id.radioButton_male);
        radioFemale = findViewById(R.id.radioButton_female);
        buttonLoginUp = findViewById(R.id.button_loginUp);
        spinner = (Spinner) findViewById(R.id.spinner_city);
        mockData();
        initAdapter();
        spinner.setAdapter(adapter);
        setlistener();
    }

    private void initAdapter() {
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void mockData() {
        dataList = new ArrayList<>();
        dataList.add("北京");
        dataList.add("上海");
        dataList.add("广州");
        dataList.add("深圳");
        dataList.add("咸宁");
    }

    private void setlistener() {
        checkBoxDance.setOnClickListener(this);
        checkBoxPlayBall.setOnClickListener(this);
        checkBoxSing.setOnClickListener(this);
        radioMale.setOnClickListener(this);
        radioFemale.setOnClickListener(this);
        buttonLoginUp.setOnClickListener(this);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                personMO.setCity(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        boolean radioChecked = false;
        boolean checkChecked = false;
        if (v instanceof RadioButton){
            radioChecked = ((RadioButton) v).isChecked();
        }
        if (v instanceof CheckBox){
            checkChecked = ((CheckBox) v).isChecked();
        }

        switch (v.getId()){
            case R.id.radioButton_male:
                if (radioChecked){
                    personMO.setSex(SexEnum.MALE.getCode());
                }
                break;
            case R.id.radioButton_female:
                if (radioChecked){
                    personMO.setSex(SexEnum.FEMALE.getCode());
                }
                break;
            case R.id.checkBox_dance:
                if (checkChecked){
                    personMO.getHobby().setLikeDance(true);
                }else {
                    personMO.getHobby().setLikeDance(false);
                }
                break;
            case R.id.checkBox_sing:
                if (checkChecked){
                    personMO.getHobby().setLikeSing(true);
                }else {
                    personMO.getHobby().setLikeSing(false);
                }
                break;
            case R.id.checkBox_playBall:
                if (checkChecked){
                    personMO.getHobby().setLikePlayBall(true);
                }else {
                    personMO.getHobby().setLikePlayBall(false);
                }
                break;
            case R.id.button_loginUp:
                String username = usernameView.getText().toString();
                String passwprd = passwordView.getText().toString();
                if (username == null){
                    personMO.setUsername("");
                }else {
                    personMO.setUsername(username);
                }
                if (passwprd == null){
                    personMO.setUsername("");
                }else {
                    personMO.setPassword(passwprd);
                }
                Intent intent = new Intent(this,DisplayActivity.class);
                intent.putExtra("Obj",new Gson().toJson(personMO));
                startActivity(intent);
                break;
        }
    }
}