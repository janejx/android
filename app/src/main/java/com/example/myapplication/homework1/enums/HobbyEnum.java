package com.example.myapplication.homework1.enums;

public enum  HobbyEnum {
    Dance(0,"跳舞"),SINGE(1,"唱歌"),PLAYBALL(2,"打球")
    ;
    public  final int code;
    public  final String message;

    HobbyEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
