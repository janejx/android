package com.example.myapplication.homework1.enums;

public enum SexEnum {
    MALE(0,"男"),FEMALE(1,"女")
    ;
    private final int code;
    private final String message;

    SexEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static String getValueByCode(Integer code){
        for(SexEnum sex:SexEnum.values()){
            if(code.equals(sex.getCode())){
                return sex.getMessage();
            }
        }
        return  null;
    }
}
