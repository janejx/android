package com.example.myapplication.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import static com.bumptech.glide.Glide.with;

/**
 * Glide工具类
 *
 */
public class GlideUtil {

    private static final String TAG = "GlideUtil";

    //加载弹窗
    private static ProgressBar progressBar;



    /**
     * 获取ImageViewTarget
     *
     * @param imageView
     * @return
     */
    private static ImageViewTarget<Drawable> getImageViewTarget(final ImageView imageView) {
        ImageViewTarget<Drawable> imageViewTarget = new ImageViewTarget<Drawable>(imageView) {
            @Override
            public void onLoadStarted(@Nullable Drawable placeholder) {
                super.onLoadStarted(placeholder);
                if(progressBar != null){
                    progressBar.setVisibility(View.VISIBLE);
                }
                Log.d(TAG, "开始加载图片");
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                if(progressBar != null){
                    progressBar.setVisibility(View.GONE);
                }
                Log.d(TAG, "加载图片失败");
            }

            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                super.onResourceReady(resource, transition);
                // 图片加载完成
                if(progressBar != null){
                    progressBar.setVisibility(View.GONE);
                }
                imageView.setImageDrawable(resource);
                Log.d(TAG, "加载图片完成");
            }


            @Override
            protected void setResource(@Nullable Drawable resource) {

            }
        };
        return imageViewTarget;
    }



    /**
     * 显示网络Url图片 附带加载网络监听和设置资源监听 显示加载弹窗
     * @param context 显示在哪个Activity/Fragment上
     * @param url  网络图片url
     * @param imageView 图片控件

     */
    public static void loadImgListenerNeedDialog(Context context, String url, ImageView imageView, ProgressBar progressBar) {
        GlideUtil.progressBar = progressBar;

            Glide.with(context)
                    .load(url)
                    .into(getImageViewTarget(imageView));
    }
}
