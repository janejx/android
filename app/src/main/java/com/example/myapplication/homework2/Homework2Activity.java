package com.example.myapplication.homework2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.util.GlideUtil;

public class Homework2Activity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageView;
    private ProgressBar progressBar;
    private Button button;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        imageView = (ImageView) findViewById(R.id.imageView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    private void loadImage(String url) {
        GlideUtil.loadImgListenerNeedDialog(this,url,imageView,progressBar);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                String url = editText.getText().toString();
                if (TextUtils.isEmpty(url)){
                    Toast.makeText(this, "Url不为空，请输入Url",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                loadImage(url);
        }
    }
}