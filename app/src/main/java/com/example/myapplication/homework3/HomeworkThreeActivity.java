package com.example.myapplication.homework3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.myapplication.R;
import com.google.android.material.navigationrail.NavigationRailView;

public class HomeworkThreeActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_three);
        NavigationRailView navigationRailView = (NavigationRailView)findViewById(R.id.navigation_rail);
        textView = (TextView) findViewById(R.id.textView);
        navigationRailView.setOnNavigationItemReselectedListener(item -> {
            switch (item.getItemId()){
                case R.id.alarms:
                    textView.setText("alarms双击");
                    break;
                case R.id.schedule:
                    textView.setText("schedule双击");
                    break;
                case R.id.timer:
                    textView.setText("timer双击");
                    break;
                case R.id.stopwatch:
                    textView.setText("stopwatch双击");
                    break;
            }
        });
        navigationRailView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.alarms:
                    textView.setText("alarms");
                    return true;
                case R.id.schedule:
                    textView.setText("schedule");
                    return true;
                case R.id.timer:
                    textView.setText("timer");
                    return true;
                case R.id.stopwatch:
                    textView.setText("stopwatch");
                    return true;
            }
            return false;
        });

    }
}