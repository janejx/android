package com.example.myapplication.homework5;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE uid IN (:userIds)")
    List<User> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user WHERE username is :username AND " +
            "password is :password LIMIT 1")
    User findByNameAndPassword(String username, String password);

    @Query("SELECT * FROM user WHERE uid is :id LIMIT 1")
    User findById(int id);

    @Update
    void update(User user);

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);
}

