package com.example.myapplication.homework5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.R;

public class Homework5Activity extends AppCompatActivity {

    private Button btWrite;
    private Button btLoad;
    private EditText etUsername;
    private EditText etPassword;
    private AppDatabase db;
    private EditText id;
    private Button btUpdate;
    private EditText upPassword;
    private EditText upUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework4);
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-name").build();
        initView();
        initListener();
    }

    private void initListener() {
        btWrite.setOnClickListener(v -> {
            User user = new User();
            user.setUsername(etUsername.getText().toString());
            user.setPassword(etPassword.getText().toString());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    db.userDao().insertAll(user);
                }
            }).start();
        });
        btLoad.setOnClickListener(v -> {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String s = id.getText().toString();
                    User user = db.userDao().findById(Integer.parseInt(s));
                    Log.e("asher", user.toString());
                }
            }).start();
        });

        btUpdate.setOnClickListener(v -> {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String s = id.getText().toString();
                    User user1 = new User();
                    user1.setUid(Integer.parseInt(s));
                    user1.setUsername(upUsername.getText().toString());
                    user1.setPassword(upPassword.getText().toString());
                    db.userDao().update(user1);

                }
            }).start();
        });
    }

    private void initView() {
        btWrite = (Button) findViewById(R.id.bt_write);
        btLoad = (Button) findViewById(R.id.bt_load);
        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);
        id = (EditText) findViewById(R.id.id);
        btUpdate = (Button) findViewById(R.id.bt_update);
        upPassword = (EditText) findViewById(R.id.up_password);
        upUsername = (EditText) findViewById(R.id.up_username);
    }
}