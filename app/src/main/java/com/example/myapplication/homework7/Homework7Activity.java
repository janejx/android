package com.example.myapplication.homework7;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class Homework7Activity extends AppCompatActivity {

    private static final String TAG = "asher";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework7);
        List<SmsData> smsList = getSmsList();
        for (SmsData i : smsList){
            Toast.makeText(this,i.toString(),Toast.LENGTH_SHORT).show();
            Log.e("asher",i.toString());
        }
    }

    public List<SmsData> getSmsList() {
        //获取所有短信的 Uri
        ArrayList<SmsData> smsList = new ArrayList<>();
        Uri uri = Uri. parse( "content://sms/");
        //获取ContentResolver对象
        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        //根据Uri 查询短信数据
        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        if ( null != cursor) {
            Log. i( TAG, "cursor.getCount():" + cursor.getCount());
            //根据得到的Cursor一条一条的添加到smsList(短信列表)中
            while (cursor.moveToNext()) {
                int _id = cursor.getInt(cursor.getColumnIndex("_id" ));
                int type = cursor.getInt(cursor.getColumnIndex("type" ));
                String address = cursor.getString(cursor.getColumnIndex( "address"));
                String body = cursor.getString(cursor.getColumnIndex("body" ));
                String date = cursor.getString(cursor.getColumnIndex("date" ));
                SmsData smsData = new SmsData(_id, type, address, body, date);
                smsList.add(smsData);
            }
            cursor.close();
        }
        return smsList;
    }
}