package com.example.myapplication.homework7;

/**
 * Time: 2021-05-31
 * Description:
 */
public class SmsData {
    private int id;
    private int type;//类型
    private String address;//发送地址
    private String body;//内容
    private String date;//发送时间

    public SmsData(int id, int type, String address, String body, String date) {
        this.id = id;
        this.type = type;
        this.address = address;
        this.body = body;
        this.date = date;
    }

    @Override
    public String toString() {
        return "SmsData{" +
                "id=" + id +
                ", type=" + type +
                ", address='" + address + '\'' +
                ", body='" + body + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
