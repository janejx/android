package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.myapplication.homework1.Homework1Activity;
import com.example.myapplication.homework2.Homework2Activity;
import com.example.myapplication.homework3.HomeworkThreeActivity;
import com.example.myapplication.homework4.Homework4Activity;
import com.example.myapplication.homework5.Homework5Activity;
import com.example.myapplication.homework6.Homework6;
import com.example.myapplication.homework7.Homework7Activity;
import com.example.myapplication.homework8.Homework8Activity;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                Intent intent = new Intent(this, Homework1Activity.class);
                startActivity(intent);
                break;
            case R.id.item2:
                Intent intent2 = new Intent(this, Homework2Activity.class);
                startActivity(intent2);
                break;
            case R.id.item3:
                Intent intent3 = new Intent(this, HomeworkThreeActivity.class);
                startActivity(intent3);
                break;
            case R.id.item4:
                Intent intent4 = new Intent(this, Homework4Activity.class);
                startActivity(intent4);
                break;
            case R.id.item5:
                Intent intent5 = new Intent(this, Homework5Activity.class);
                startActivity(intent5);
                break;
            case R.id.item6:
                Intent intent6 = new Intent(this, Homework6.class);
                startActivity(intent6);
                break;
            case R.id.item7:
                Intent intent7 = new Intent(this, Homework7Activity.class);
                startActivity(intent7);
                break;
            case R.id.item8:
                Intent intent8 = new Intent(this, Homework8Activity.class);
                startActivity(intent8);
                break;

        }
        return true;
    }
}