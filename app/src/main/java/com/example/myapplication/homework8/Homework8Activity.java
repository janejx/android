package com.example.myapplication.homework8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;

import java.util.ArrayList;

public class Homework8Activity extends AppCompatActivity {

    private EditText phone;
    private EditText content;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework8);
        phone = (EditText) findViewById(R.id.et_phone);
        content = (EditText) findViewById(R.id.et_content);
        button = (Button) findViewById(R.id.bt_send);
        button.setOnClickListener(v -> {

            sendMessage(phone.getText().toString(),content.getText().toString());
            Toast.makeText(this,"success",Toast.LENGTH_SHORT).show();
        });
    }

    private void sendMessage(String number, String content) {
        ArrayList<String> messages = SmsManager.getDefault().divideMessage(content);
        for (String text : messages) {
            SmsManager.getDefault().sendTextMessage(number, null, text, null, null);
        }
    }
}